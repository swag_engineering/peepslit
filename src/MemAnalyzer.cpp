#include "MemAnalyzer.hpp"
#include <filesystem>
#include <unistd.h>
#include <regex>

MemAnalyzer& MemAnalyzer::getInstance()
{
	static MemAnalyzer analyzer;
	return analyzer;
}

MemAnalyzer::MemAnalyzer()
	: saveBuffer_{nullptr},
	initialized_{false}
{
}

void MemAnalyzer::init(const std::string pidName)
{
	std::vector<std::string> pids;
	std::vector<std::string> names;
	std::regex rgxPid("\\d+");
	for (const auto &entry : std::filesystem::directory_iterator("/proc"))
	{
		std::string filename = entry.path().stem().string();
		if(!std::regex_match(filename, rgxPid))
			continue;
		std::ifstream ifs((entry.path() / "comm").string());
		std::string name(std::istreambuf_iterator<char>{ifs}, {});
		pids.push_back(filename);
		names.push_back(name);
	}

	std::string pid;
	std::string name;
	if(std::regex_match(pidName.begin(), pidName.end(), rgxPid))
	{
		std::vector<std::string>::iterator it = std::find(pids.begin(), pids.end(), pidName);
		if (it == pids.end())
			throw std::runtime_error("Process with pid '" + pidName + "' is not running");
		pid = pidName;
		size_t idx = std::distance(pids.begin(), it);
		name = names[idx].substr(0, names[idx].size()-1);
	} else {
		for(size_t i=0; i<names.size(); i++)
		{
			if(names[i].substr(0, names[i].size()-1) != pidName)	// delete \n in the end
				continue;
			if(pid != "")
				throw std::runtime_error("Found multiple processes with name '" +
					pidName + "'. Try to use PID.");
			pid = pids[i];
		}
		if(pid == "")
			throw std::runtime_error("Process with name '" + pidName + "' is not running");
		name = pidName;
	}
	name_ = name;
	pid_ = std::stoul(pid);
	if(pid_ == getpid())
		throw std::runtime_error("Just don't");
	updateInfo();
	memSream_ =  std::ifstream("/proc/" + std::to_string(pid_) + "/mem", std::ios::in | std::ios::binary);
}

MemAnalyzer::~MemAnalyzer()
{
	deallocateBuffer();
}

void MemAnalyzer::updateInfo()
{
	collectMemoryRange();
	allocateBuffer();
}

void MemAnalyzer::collectMemoryRange()
{
	std::regex rgxHeap("([0-9a-f]{8,16})-([0-9a-f]{8,16}) rw-p [0]{8} 00:00 0 *\\[heap\\]");
	std::smatch matches;
	bool found = false;

	std::ifstream maps("/proc/" + std::to_string(pid_) + "/maps");
	std::string line;

	while(getline(maps, line))
		if(std::regex_search(line, matches, rgxHeap))
		{
			heapStart_ = stoul(matches[1].str(), 0, 16);
			heapLen_ = stoul(matches[2].str(), 0, 16) - heapStart_;
			found = true;
		}

	if(!found)
		throw std::runtime_error("Unable to find heap");
}

void MemAnalyzer::allocateBuffer()
{
	deallocateBuffer();
	memBuffer_ = new uint8_t[heapLen_];
}

void MemAnalyzer::deallocateBuffer()
{
	if(memBuffer_ != nullptr)
	{
		delete [] memBuffer_;
		memBuffer_ = nullptr;
	}
}

void MemAnalyzer::fillBuffer()
{
	readMem(memBuffer_, heapStart_, heapLen_);
}

void MemAnalyzer::reset()
{
	results_.clear();
	initialized_ = false;
}

void MemAnalyzer::dumpResult(std::string filename, size_t offset,
	size_t left, size_t right, bool support)
{
	size_t start = heapStart_ + offset - left*sizeof(uint32_t);
	uint64_t pos = (uint64_t)left;
	size_t dataSize = left + right + 1;		// 1 for result value itself
	size_t dumpSize = dataSize + \
		(support ?  sizeof(dumpPrefix)/sizeof(uint32_t) +
			sizeof(pos)/sizeof(uint32_t) : 0);
	uint32_t* dumpBuf = new uint32_t[dumpSize];

	uint32_t* tmp = dumpBuf;
	if(support)
	{
		memcpy(tmp, &dumpPrefix, sizeof(dumpPrefix));
		tmp += sizeof(dumpPrefix)/sizeof(uint32_t);
		memcpy(tmp, &pos, sizeof(pos));
		tmp += sizeof(pos)/sizeof(uint32_t);
	}
	readMem(tmp, start, dataSize);

	std::fstream fs;
	fs.open(filename.c_str(), std::ios::out | std::ios::binary | std::ios::trunc);
    fs.write(reinterpret_cast<const char*>(dumpBuf), dumpSize*sizeof(uint32_t));
    fs.close();
}

void MemAnalyzer::findVolatile(bool volatileSearch, bool initValues)
{
	if(initValues)
	{
		saveBuffer_ = new uint32_t[heapLen_/sizeof(uint32_t)];
		readMem(saveBuffer_, heapStart_, heapLen_/sizeof(uint32_t));
		return;
	} else if(!saveBuffer_) {
		throw std::runtime_error("Values buffer was not initialized");
	}

	size_t cnt = 0;
	std::vector<size_t> tmpAddr;
	uint32_t* tmpBuffer;
	size_t resultsSize = initialized_ ? results_.size() : heapLen_/sizeof(uint32_t);
	tmpBuffer = new uint32_t[resultsSize];
	fillBuffer();
	uint32_t* haystack = reinterpret_cast<uint32_t*>(memBuffer_);

	// this workaround is done to save some time and RAM that would be needed to
	// preinitialize results_ vector
	for (size_t n=0; n < resultsSize; n++)
	{
		uint32_t value = initialized_ ? \
			haystack[results_[n]/sizeof(uint32_t)] : haystack[n];
		if((!volatileSearch && saveBuffer_[n] == value) ||
			(volatileSearch && saveBuffer_[n] != value))
		{
			tmpBuffer[cnt] = value;
			tmpAddr.push_back(initialized_ ? results_[n] : n*sizeof(uint32_t));
			cnt++;
		}
	}
	if(!initialized_)
		initialized_ = true;
	results_ = tmpAddr;
	delete saveBuffer_;
	saveBuffer_ = tmpBuffer;
}