#include "MemInspectorWindow.hpp"
#include "ResultListWidgetItem.hpp"
#include "MemInspectorTable.hpp"
#include "MemInspectorDelegate.hpp"
#include <QtWidgets>

// TODO make window's height resizable and maximized, but forbid
// width change, it should be changed only programatically

// There are couple of places in this code that I'm not proude of,
// I was not able to implement something in a way I wanted
// due to QT Model-View implementation

MemInspectorWindow::MemInspectorWindow(size_t seedAddr, QWidget* parent)
	: QDialog(parent),
	numPreloadedRows_{40},	// to future settings
	numRowsToInsert_{4} 	//should be odd
{
	setWindowTitle ("Memory inspector");
	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->setSizeConstraint(QLayout::SetFixedSize);

	QHBoxLayout* updateLayout = new QHBoxLayout;
	mainLayout->addLayout(updateLayout);
	updateLayout->setAlignment(Qt::AlignCenter);
	QLabel* updateLabel = new QLabel("Update rate:");
	updateLayout->addWidget(updateLabel);
	QDoubleSpinBox* updateSpinBox = new QDoubleSpinBox;
	updateLayout->addWidget(updateSpinBox);
	updateSpinBox->setSuffix("s");
	updateSpinBox->setRange(0, 5);
	updateSpinBox->setSingleStep(0.5);
	updateSpinBox->setValue(0.5);
	connect(updateSpinBox, SIGNAL(valueChanged(double)), this, SLOT(onUpdateRateChange(double)));


	QVBoxLayout* viewLayout = new QVBoxLayout;
	mainLayout->addLayout(viewLayout);
	tableView_ = new QTableView;
	viewLayout->addWidget(tableView_);
	table_ = new MemInspectorTable(seedAddr, tableView_, numPreloadedRows_); // 40 should be const and keep alignment

	updatTimer_ = new QTimer(this);
	connect(updatTimer_, SIGNAL(timeout()), table_, SLOT(update()));
	updatTimer_->setInterval(updateSpinBox->value()*1000);
	updatTimer_->start();

	size_t baseAddr = table_->getBaseAddr();
	size_t startSpanRow = (baseAddr%sizeof(uint64_t))/sizeof(uint32_t);
	tableNotAligned_ = (bool)startSpanRow;
	for(size_t i=startSpanRow; i<(size_t)table_->rowCount()-1; i+=2)
		tableView_->setSpan(i, table_->getPointerColumnIdx(), 2, 1);

	tableView_->setModel(table_);
	tableView_->verticalHeader()->hide();
	tableView_->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	tableView_->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
	tableView_->setEditTriggers(QAbstractItemView::NoEditTriggers);
	tableView_->setSelectionMode(QAbstractItemView::NoSelection);
	tableView_->setAlternatingRowColors(true);
	tableView_->setStyleSheet("alternate-background-color: #d4efff;background-color: #f2fbff;");
	tableView_->setItemDelegate(new MemInspectorDelegate(this));
	connect(tableView_, SIGNAL(doubleClicked(const QModelIndex&)),
		this, SLOT(onTableClicked(const QModelIndex&)));

	slider_ = tableView_->verticalScrollBar();

	size_t width = 0;
	for(int i=0; i<table_->columnCount(); i++)
		width += table_->getColumnWidth(i);

	tableView_->setFixedSize(width + slider_->sizeHint().width() + 10, 700);
	connect(slider_, SIGNAL(valueChanged(int)), this, SLOT(sliderChanged(int)));
}

void MemInspectorWindow::showEvent(QShowEvent* event)
{
	QDialog::showEvent(event);
	changeSliderPos((float)table_->getFocusRow()/table_->rowCount());
}


//continue here
void MemInspectorWindow::sliderChanged(int value)
{
	float posProc = (float)value/(slider_->maximum() - slider_->minimum());
	size_t rowCount = table_->rowCount();
	size_t rowLeft = posProc < 0.5 ? rowCount*posProc : rowCount*(1-posProc);

	if(rowLeft < numRowsToInsert_ && posProc < 0.5) // insert at start
	{
		size_t inserted = table_->insertAtStart(numRowsToInsert_);
		changeSliderPos((float)inserted/rowCount);

		for(size_t i = (size_t)tableNotAligned_; i < inserted; i += 2)
			tableView_->setSpan(i, table_->getPointerColumnIdx(), 2, 1);
	}
	if(rowLeft < numRowsToInsert_ && posProc > 0.5) // insert in the end
	{
		table_->insertInTheEnd(numRowsToInsert_);

		size_t start = table_->rowCount() - (numRowsToInsert_ + (size_t)tableNotAligned_);
		size_t stop = table_->rowCount() - 1;
		for(size_t i=start; i<stop; i+=2)
			tableView_->setSpan(i, table_->getPointerColumnIdx(), 2, 1);
	}
}


void MemInspectorWindow::changeSliderPos(float proc)
{
	// proc in range [0, 1]
	slider_->blockSignals(true);
	slider_->setSliderPosition((slider_->maximum() - slider_->minimum())*proc);
	slider_->blockSignals(false);
}

void MemInspectorWindow::onTableClicked(const QModelIndex& index)
{
	if((size_t)index.column() == table_->getPointerColumnIdx() &&
				table_->addrInHeapRange(table_->getQStringValue(index)))
	{
		QString valStr = table_->getQStringValue(index);
		bool ok;
		size_t val = valStr.toULong(&ok, 16);
		MemInspectorWindow* inspectorWindow = new MemInspectorWindow(val);	
		inspectorWindow->setAttribute(Qt::WA_DeleteOnClose);
		inspectorWindow->show();
	}
}

void MemInspectorWindow::onUpdateRateChange(double value)
{
	if(value)
	{
		updatTimer_->setInterval(value*1000);	// from s to ms
		updatTimer_->start();
	} else {
		updatTimer_->stop();
	}
}