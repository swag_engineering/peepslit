#include "SearchWindow.hpp"
#include "ISearchFrame.hpp"
#include "ExactSearchFrame.hpp"
#include "InRangeSearchFrame.hpp"
#include "RegexSearchFrame.hpp"
#include "VolatilitySearchFrame.hpp"
#include "ResultListWidgetItem.hpp"
#include "MemInspectorWindow.hpp"
#include "MemAnalyzer.hpp"
#include <QtWidgets>
#include <string>


SearchWindow::SearchWindow(QWidget* parent)
	: QDialog(parent)
{
	setWindowTitle ("Search in heap");
	setFixedSize(QSize(280, 420));

	QVBoxLayout* mainLayout = new QVBoxLayout(this);

	QHBoxLayout* selectorLayout = new QHBoxLayout;
	mainLayout->addLayout(selectorLayout);

	searchSelector_ = new QComboBox;
	searchSelector_->setFixedSize(QSize(160, 30));
	selectorLayout->addWidget(searchSelector_, Qt::AlignCenter);


	searchSelectionMap_["Exact search"] = &createFrameInstance<ExactSearchFrame>;
	searchSelectionMap_["Search in range"] = &createFrameInstance<InRangeSearchFrame>;
	searchSelectionMap_["Regex search"] = &createFrameInstance<RegexSearchFrame>;
	searchSelectionMap_["Volatility search"] = &createFrameInstance<VolatilitySearchFrame>;
	for (auto const& element : searchSelectionMap_)
		searchSelector_->addItem(element.first.c_str());
	searchSelector_->setCurrentIndex(searchSelector_->findText("Exact search"));

  	frameLayout_ = new QHBoxLayout;
  	frameLayout_->setAlignment(Qt::AlignCenter);
	mainLayout->addLayout(frameLayout_);

	curFrame_ = searchSelectionMap_[std::string("Exact search")]();
	frameLayout_->addWidget(curFrame_);
	connect(curFrame_, SIGNAL(resultsUpdated()), this, SLOT(updateResults()));
	connect(searchSelector_, SIGNAL(currentIndexChanged(QString)),
		this, SLOT(searchSelectorChanged(QString)));

	resultsList_ = new QListWidget;
	mainLayout->addWidget(resultsList_);
	connect(resultsList_, SIGNAL(itemSelectionChanged()), this, SLOT(focusChanged()));
	connect(resultsList_, SIGNAL(itemDoubleClicked(QListWidgetItem*)),
		this, SLOT(openMemInspector(QListWidgetItem*)));

	QHBoxLayout* saveLayout = new QHBoxLayout;
	saveLayout->setAlignment(Qt::AlignCenter);
	mainLayout->addLayout(saveLayout);

	saveLineEdit_ = new QLineEdit;
	saveLineEdit_->setPlaceholderText("Label");
	saveLineEdit_->setFixedSize(QSize(100, 30));
	saveLineEdit_->setValidator(new QRegExpValidator(QRegExp("[a-zA-Z_][a-zA-Z0-9_]*"), this));
	saveLayout->addWidget(saveLineEdit_);

	saveButton_ = new QPushButton("Save");
	saveButton_->setFixedSize(QSize(50, 30));
	saveLayout->addWidget(saveButton_);
	connect(saveButton_, SIGNAL(clicked()), this, SLOT(saveButtonClicked()));

	activateSaving(false);
}

void SearchWindow::searchSelectorChanged(QString searchType)
{
	frameLayout_->removeWidget(curFrame_);
	delete curFrame_;
	curFrame_ = searchSelectionMap_[searchType.toUtf8().constData()]();
	frameLayout_->insertWidget(frameLayout_->count()-1, curFrame_);	
	connect(curFrame_, SIGNAL(resultsUpdated()), this, SLOT(updateResults()));
}

void SearchWindow::activateSaving(bool val)
{
	saveButton_->setEnabled(val);
	saveLineEdit_->setEnabled(val);
}

void SearchWindow::updateResults()
{
	resultsList_->clear();
	std::vector<size_t> results = MemAnalyzer::getInstance().getResults();
	size_t size = (results.size() > 1000 ? 1000 : results.size());
	for(size_t i = 0; i < size; i++)
		resultsList_->addItem(new ResultListWidgetItem(
			QString::number(results[i]), "", resultsList_));
}


void SearchWindow::focusChanged()
{
	activateSaving(!resultsList_->selectedItems().isEmpty());
}

void SearchWindow::saveButtonClicked()
{
	emit saveResult(new ResultListWidgetItem(resultsList_->selectedItems()[0]->text(),
		saveLineEdit_->text()));
}

void SearchWindow::openMemInspector(QListWidgetItem* item)
{
	MemInspectorWindow* inspectorWindow = new MemInspectorWindow(
		static_cast<ResultListWidgetItem*>(item)->getAbsPosition(), this);	
	inspectorWindow->setAttribute(Qt::WA_DeleteOnClose);
	inspectorWindow->show();
}