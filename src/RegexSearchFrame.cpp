#include "RegexSearchFrame.hpp"
#include "MemAnalyzer.hpp"
#include <QtWidgets>
#include <sstream>
#include <regex>
#include <string>


RegexSearchFrame::RegexSearchFrame(QWidget* parent)
	: ISearchFrame(parent)
{

	QVBoxLayout* mainLayout = new QVBoxLayout(this);

	searchSeq_ = new QTextEdit;
	searchSeq_->setFixedHeight(60);
	searchSeq_->setPlaceholderText("Comma separated sequence");
	mainLayout->addWidget(searchSeq_);

	QHBoxLayout* buttonsLayout = new QHBoxLayout;
	mainLayout->addLayout(buttonsLayout);

	findButton_ = new QPushButton("Find");
	findButton_->setFixedSize(QSize(50, 30));
	buttonsLayout->addWidget(findButton_);
	connect(findButton_, SIGNAL(clicked()), this, SLOT(findResults()));

	QPushButton* resetButton = new QPushButton("Reset");
	resetButton->setFixedSize(QSize(50, 30));
	buttonsLayout->addWidget(resetButton);
	connect(resetButton, SIGNAL(clicked()), this, SLOT(resetResults()));

	resultsLable_ = new QLabel("Results found: 0");
	resultsLable_->setAlignment(Qt::AlignBottom);
	mainLayout->addWidget(resultsLable_);
}

// rewrite with qt classes
void RegexSearchFrame::findResults()
{
	std::vector<uint32_t> seq;
	std::string seqStr = searchSeq_->toPlainText().toStdString();
	if("" == seqStr)
	{
		QMessageBox msgBox;
		msgBox.setText("Write sequence to search");
		msgBox.exec();
		return;
	} else if(!std::regex_match(seqStr, std::regex(
			"(((?:0x[0-9a-f]+)|(?:\\d+)|x),[ |\\n]*)*((?:0x[0-9a-f]+)|(?:\\d+)|x)"))) {
		QMessageBox msgBox;
		msgBox.setText("Bad formatted sequence");
		msgBox.exec();
		return;
	}
	std::smatch sm;
	while(std::regex_search(seqStr, sm, std::regex("((?:0x[0-9a-f]+)|(?:\\d+)|x)")))
	{
		if(sm[0] == 'x')
		{
			seq.push_back(MemAnalyzer::getInstance().anyNumber);
		} else if(sm[0].str().find("0x") != std::string::npos) {
			uint32_t x;
			std::stringstream stream;
			stream << std::hex << sm[0].str().substr(2);
			stream >> x;
			seq.push_back(x);
		} else {
			seq.push_back(std::stoi(sm[0]));
		}
		seqStr = sm.suffix().str();
	}
	MemAnalyzer::getInstance().findSequence(seq);
	size_t size = MemAnalyzer::getInstance().getResultsSize();
	resultsLable_->setText("Results found: " + QString::number(size));
	if(size == 0)
		findButton_->setEnabled(false);
	emit resultsUpdated();
}

void RegexSearchFrame::resetResults()
{
	MemAnalyzer::getInstance().reset();
	resultsLable_->setText("Results found: 0");
	findButton_->setEnabled(true);
	emit resultsUpdated();
}