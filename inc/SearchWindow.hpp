#ifndef PEEPSLIT_SEARCH_WINDOW_H
#define PEEPSLIT_SEARCH_WINDOW_H

#include <QDialog>

class QHBoxLayout;
class ISearchFrame;
class QFrame;
class QComboBox;
class QListWidget;
class QLineEdit;
class QPushButton;
class QListWidgetItem;
class ResultListWidgetItem;
class MemInspectorWindow;

template<typename T> ISearchFrame* createFrameInstance() { return new T; }

class SearchWindow : public QDialog
{
	Q_OBJECT
public:
	SearchWindow(QWidget* parent = nullptr);

private:
	QHBoxLayout* frameLayout_;
	QComboBox* searchSelector_;
	ISearchFrame* curFrame_;
	QListWidget* resultsList_;
	QLineEdit* saveLineEdit_;
	QPushButton* saveButton_;

	std::map<std::string, ISearchFrame*(*)()> searchSelectionMap_;

	void activateSaving(bool val);

public slots:
	void searchSelectorChanged(QString searchType);
	void saveButtonClicked();
	void updateResults();
	void focusChanged();
	void openMemInspector(QListWidgetItem* item);

signals:
	void saveResult(ResultListWidgetItem* item);

};

#endif