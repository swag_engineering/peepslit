# Peepslit

### Abstract
Initial intention was to build something like CheatEngine but for Linux systems. However during development focus changed a bit and I hope that now program could be evaluated as minimalistic memory forensics tool. For more information please refer to our [Wiki page](https://gitlab.com/swag_engineering/peepslit/-/wikis/home). 

### Functionality
     - raw heap observation (hex32, hex64, int32, float, char)
     - 4 types of search in heap
     - pointers detection
     - dumps creation
     - dumps comparison (hex32, int32, float, char) with alignment and highlight support

### Requirements
Project was created and tested under Ubuntu 18.10. I also plan to add Manjaro and Mac support.  
Here the versions I use to build the project:  
    - Qt version 5.12.4 (qmake version 3.1)  
    - c\++17 (g++ version 9.2.1)  
    - make version 4.2.1  

### Build
``` shell
$ cd peepslit
$ qmake -o Makefile peepslit.pro && make
$ sudo ./build/bin/peepslit
```
You have to run it with sudo to be able to read other procese's memory file.

### Known problems
1) Use g++ instead of clang. From StackOverflow:  
    Libc++, which is the C++ standard library on OS X, has not moved <experimental/filesystem> to <filesystem> yet because the specification is not stable.
