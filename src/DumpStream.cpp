#include "DumpStream.hpp"


DumpStream::DumpStream(QString filename)
	: aimPos_{0}, nanOffset_{0}
{
	stream_ = std::ifstream(filename.toStdString(), std::ios::in | std::ios::binary | std::ios::ate);
	uint64_t prefix;
	dataSize_ = stream_.tellg() / sizeof(int32_t);
	// assumption, change to 0 if wrong
	baseOffset_ = (sizeof(focusDistance_) + sizeof(dumpPrefix_)) / sizeof(uint32_t);
	stream_.seekg(0);
	if(dataSize_ > baseOffset_)
	{
		stream_.read((char*)&prefix, sizeof(prefix));
	}
	if(prefix == dumpPrefix_)
	{
		stream_.read((char*)&aimPos_, sizeof(focusDistance_));
		dataSize_ -= baseOffset_;
		if(aimPos_ > dataSize_)
			throw std::runtime_error("File seems to be corrupted");
	} else {
		baseOffset_ = 0;
		stream_.seekg(0);
	}
}

bool DumpStream::isSupported() const
{
	return (bool)baseOffset_;
}

void DumpStream::setOffset(const size_t offset)
{
	nanOffset_ = offset;
}

size_t DumpStream::getFocusPos() const
{
	return (size_t)aimPos_;
}

std::optional<uint32_t> DumpStream::operator[](size_t idx)
{
	if(idx < nanOffset_ || idx >= dataSize_ + nanOffset_)
	{
		return std::nullopt;
	}
	uint32_t value;	
	stream_.seekg((baseOffset_ + (idx - nanOffset_))*sizeof(uint32_t));
	stream_.read((char*)&value, sizeof(value));
	return value;

}

size_t DumpStream::size() const
{
	return dataSize_ + nanOffset_;
}