#ifndef PEEPSLIT_SEARCH_FRAME_PATTERN_H
#define PEEPSLIT_SEARCH_FRAME_PATTERN_H

#include <QFrame>

class ISearchFrame : public QFrame {
	Q_OBJECT
public:
	ISearchFrame(QWidget* parent=nullptr) : QFrame(parent)
		{ setFixedSize(QSize(width_, height_)); }
	// ~ISearchFrame() = delete;

	size_t width() { return width_; }
	size_t height() { return height_; }
	virtual void findResults() = 0;
	virtual void resetResults() = 0;

signals:
	void resultsUpdated();

private:
	size_t width_ = 270;
	size_t height_ = 160;

};

#endif