#ifndef PEEPSLIT_DUMPS_COMPARISON_HEADER_H
#define PEEPSLIT_DUMPS_COMPARISON_HEADER_H

#include <QtWidgets>

class DumpsComparisonHeader : public QHeaderView
{
	Q_OBJECT
public:
	DumpsComparisonHeader(QWidget* parent = 0);
	void relocateButtons();

signals:
	void closeClicked(int idx);

private:
	QPixmap closeIcon_;
	QVector<QPushButton*> btns_;

	void showEvent(QShowEvent *e) override;
public slots:
	void onCloseClicked();
	
};


#endif