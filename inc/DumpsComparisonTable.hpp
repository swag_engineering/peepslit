#ifndef PEEPSLIT_DUMPS_COMPARISON_TABLE_H
#define PEEPSLIT_DUMPS_COMPARISON_TABLE_H

// 3 scenarios:
// 1) at least one file has no signature -> show msg + no alignment
// 2) files are of different size -> show msg + align
// 3) all files have signature, same size, same aim offset -> align

#include "DumpStream.hpp"
#include <QAbstractTableModel>
#include <map>
#include <optional>


class DumpsComparisonTable : public QAbstractTableModel
{
	Q_OBJECT
public:
	DumpsComparisonTable(QObject *parent=nullptr);
	// ~DumpsComparisonTable();
	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
	QVariant headerData(int section, Qt::Orientation orientation,
		int role = Qt::DisplayRole) const override;

	QList<QStringList> collectData(size_t pos, size_t count);
	bool alignmentSupported() { return !bool(notSupportedFiles_.size()); };
	QStringList notSupportedFiles() { return notSupportedFiles_; };
	void align();
	void breakAlignment();
	void addFile(QString file);
	bool removeColumn(int column,
		const QModelIndex &parent = QModelIndex());
	size_t typesNumber() const { return types_.size(); };
	bool aligned() const { return aligned_; };
	bool highlighted() const { return highlight_; };
	size_t focusRow() const { return focusRow_*types_.size(); };
	size_t getColumnWidth(size_t idx) const;
	bool isEmpty(const QModelIndex &index) const;	
	void setHighlight(bool highlight);
	std::optional<bool> rowIsEqual(size_t row) const;
	size_t insertAtStart(size_t count);
	size_t insertInTheEnd(size_t count);
	void fixLayout() { emit layoutChanged(); };

signals:
	void alignmentPossible(bool status);


private:
	template <typename T>
	QString convert(std::optional<uint32_t> value) const;
	QString toHexString(std::optional<uint32_t> value) const;
	QString toChar(std::optional<uint32_t> value) const;
	void findCommonPathPrefix();
	void cutHeaderText(QString& text) const;

	QList<QStringList> strData_;
	std::map<QString, DumpStream> streams_;
	QStringList notSupportedFiles_;
	size_t tableSize_;
	size_t basePos_;
	bool aligned_;
	bool highlight_;
	size_t focusRow_;
	std::optional<QString> pathPrefix_;

	const QStringList types_ = {
		"hex", "int32_t", "float", "char"
	};

	QVector<QString> header_ = {"type"};

	typedef QString (DumpsComparisonTable::*pfunc)(std::optional<uint32_t>) const;
	const std::map<QString, pfunc> typeToFuncMap_ =
	{
		{"hex", &DumpsComparisonTable::toHexString},
		{"int32_t", &DumpsComparisonTable::convert<int32_t>},
		{"float", &DumpsComparisonTable::convert<float>},
		{"char", &DumpsComparisonTable::toChar}
	};

};


template <typename T>
QString DumpsComparisonTable::convert(std::optional<uint32_t> value) const
{	
	if(!value.has_value())
		return QString();
	uint32_t* ptr = &(value.value());
	return value.has_value() ? QString::number(*(reinterpret_cast<T*>(ptr))) : QString();
}

#endif