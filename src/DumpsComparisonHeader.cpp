#include "DumpsComparisonHeader.hpp"
#include "DumpsComparisonTable.hpp"


// add style change on hover
DumpsComparisonHeader::DumpsComparisonHeader(QWidget* parent)
	: QHeaderView(Qt::Horizontal, parent)
{
	closeIcon_ = QPixmap("./icons/close_icon.png").scaled(QSize(15, 15));
	setDefaultAlignment(Qt::AlignLeft);
}


void DumpsComparisonHeader::showEvent(QShowEvent*)
{
	for(int i = 1; i < count(); i++)
	{
		QPushButton* btn = new QPushButton(this);
		btn->setIcon(closeIcon_);
		btn->setStyleSheet("border:none");
		btns_.append(btn);
		connect(btn, SIGNAL(clicked()), this, SLOT(onCloseClicked()));
		btn->show();
	}
	relocateButtons();
}

void DumpsComparisonHeader::relocateButtons()
{
	size_t size = sectionSize(0);
	for(int i = 0; i < btns_.size(); i++)
	{
		size += sectionSize(i + 1);
		btns_[i]->setGeometry(size - height() * 1.2 * 0.6,
			height() * 0.2, height() * 0.6, height() * 0.6);
	}
}

void DumpsComparisonHeader::onCloseClicked()
{
	int idx = btns_.indexOf(qobject_cast<QPushButton*>(sender()));
	delete btns_[idx];
	btns_.erase(btns_.begin() + idx);
	emit closeClicked(idx + 1);
	relocateButtons();
}
