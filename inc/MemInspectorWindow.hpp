#ifndef MEM_RESULT_MEM_INSPECTOR_WINDOW_H
#define MEM_RESULT_MEM_INSPECTOR_WINDOW_H


#include <QDialog>

class MemInspectorTable;
class ResultListWidgetItem;
class QTableView;
class QScrollBar;
class QLabel;
class QDoubleSpinBox;
class QTimer;

class MemInspectorWindow: public QDialog {
Q_OBJECT
public:
	MemInspectorWindow(size_t seedAddr, QWidget* parent=nullptr);

private:
	QDoubleSpinBox* updateSpinBox_;
	MemInspectorTable* table_;
	QTableView* tableView_;
	QScrollBar* slider_;
	QTimer* updatTimer_;
	bool tableNotAligned_;		// not aligned to 64 bits
	const size_t numPreloadedRows_;
	const size_t numRowsToInsert_;

	void showEvent(QShowEvent* event);
	void changeSliderPos(float proc);


public slots:
	void sliderChanged(int value);
	void onTableClicked(const QModelIndex&);
	void onUpdateRateChange(double value);
	
};


#endif