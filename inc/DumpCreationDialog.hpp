#ifndef PEEPSLIT_DUMP_CREATION_DIALOG_H
#define PEEPSLIT_DUMP_CREATION_DIALOG_H

#include <QDialog>

class QLineEdit;
class QCheckBox;
class QPushButton;
class QString;
class ResultListWidgetItem;

class DumpCreationDialog : public QDialog
{
	Q_OBJECT
public:
	DumpCreationDialog(ResultListWidgetItem* item, QWidget *parent = nullptr);
	// ~DumpCreationDialog();

public slots:
	void dumpData();

private:
	QLineEdit* prependLineEdit_;
	QLineEdit* appendLineEdit_;
	QCheckBox* supportCheckBox_;
	QPushButton* dumpButton_;
	QString nameHint_;
	size_t heapOffset_;

	
};


#endif