#ifndef PEEPSLIT_DUMPS_COMPARISON_DELEGATE_H
#define PEEPSLIT_DUMPS_COMPARISON_DELEGATE_H

#include <QStyledItemDelegate>
#include <QSize>


class DumpsComparisonDelegate : public QStyledItemDelegate
{
	Q_OBJECT
public:
	DumpsComparisonDelegate(QObject* parent = 0)
		: QStyledItemDelegate(parent) {};
	
	void initStyleOption(QStyleOptionViewItem *option,
		const QModelIndex &index) const;

	QSize sizeHint( const QStyleOptionViewItem& option,
		const QModelIndex& index ) const;

private:
	// bad syntax, move to future settings
	QColor alternateOne = "#f2fbff";
	QColor alternateTwo = "#d4efff";
	QColor focus = "#69b7ff";
	QColor alternateWrongOne = "#ffd9d9";
	QColor alternateWrongTwo = "#ffcbc9";
	QColor alternateRightOne = "#e6ffd9";
	QColor alternateRightTwo = "#d9ffc4";
	QColor focusWrong = "#ff6e6b";
	QColor focusRight = "#5eff61";

};

#endif
