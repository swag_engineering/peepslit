#ifndef PEEPSLIT_MEM_INSPECTOR_TABLE_H
#define PEEPSLIT_MEM_INSPECTOR_TABLE_H

#include <QAbstractTableModel>
#include <cstddef>
#include <map>


class ResultListWidgetItem;
class QString;
class QVariant;
class QModelIndex;
class QTableView;

class MemInspectorTable : public QAbstractTableModel
{
	Q_OBJECT
public:
	MemInspectorTable(size_t seedAddr, QObject *parent=nullptr, size_t initRowCount=60);

	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
	QVariant headerData(int section, Qt::Orientation orientation,
		int role = Qt::DisplayRole) const override;
	size_t getFocusRow() const { return focusRow_; };
	size_t getBaseAddr() const { return baseAddr_; };
	size_t getColumnWidth(size_t idx) const { return columnsWidth_.at(header_[idx]); };
	size_t width() const;
	size_t insertAtStart(size_t count);
	size_t insertInTheEnd(size_t count);
	size_t getPointerColumnIdx() const { return header_.indexOf("uint64_t"); };
	bool addrInHeapRange(QString addr) const;
	QString getQStringValue(const QModelIndex &index) const;


public slots:
	void update();

private:
	size_t heapStart_;
	size_t heapEnd_;
	size_t baseAddr_;
	size_t focusRow_;
    QList<QStringList> strData_;
    size_t rowsNumber_;

	QList<QString> header_ = {"Address", "Offset", "uint32_t", \
		"uint32_hex", "float", "char", "uint64_t"};
	const std::map<const QString, const size_t> columnsWidth_ =
	{
		{"Address", 130},
		{"Offset", 100},
		{"uint32_t", 110},
		{"uint32_hex", 110},
		{"float", 110},
		{"char", 60},
		{"uint64_t", 160}
	};
	typedef QString (MemInspectorTable::*pfunc)(size_t, uint32_t*) const;
	std::map<QString, pfunc> typeSelectionMap_ =
	{
		{"Address", &MemInspectorTable::getAddrHex},
		{"Offset", &MemInspectorTable::getOffset},
		{"uint32_t", &MemInspectorTable::getValue<uint32_t>},
		{"uint32_hex", &MemInspectorTable::getValueHex<uint32_t>},
		{"float", &MemInspectorTable::getValue<float>},
		{"char", &MemInspectorTable::getChar},
		{"uint64_t", &MemInspectorTable::getValueHex<uint64_t>}
	};


	template <typename T>
	QString getValueHex(size_t addr, uint32_t* value) const;
	template <typename T>
	QString getValue(size_t addr, uint32_t* value) const;
	QString getOffset(size_t addr, uint32_t* value) const;
	QString getAddrHex(size_t addr, uint32_t* value) const;
	QString getChar(size_t addr, uint32_t* value) const;
	QList<QStringList> collectData(size_t addr, size_t count);
	
};

template <typename T>
QString MemInspectorTable::getValueHex(size_t, uint32_t* value) const
{
	return "0x" + QString::number(*(reinterpret_cast<T*>(value)), 16).rightJustified(sizeof(T)*2, '0');
}

template <typename T>
QString MemInspectorTable::getValue(size_t, uint32_t* value) const
{
	return QString::number(*(reinterpret_cast<T*>(value)));
}


#endif