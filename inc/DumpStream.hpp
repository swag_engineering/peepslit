#ifndef PEEPSLIT_DUMP_STREAM_H
#define PEEPSLIT_DUMP_STREAM_H

#include <QString>
#include <fstream>
#include <optional>


class DumpStream
{
public:
	DumpStream(QString filename);
	std::optional<uint32_t> operator[](size_t idx);
	void setOffset(const size_t offset);
	bool isSupported() const;
	size_t getFocusPos() const;
	size_t size() const;

private:
	static const uint64_t dumpPrefix_ = 0x0000aabbccddeeff; // move to settings
	static const uint64_t focusDistance_ = 0; // move to settings
	size_t aimPos_;
	std::ifstream stream_;
	size_t nanOffset_;
	size_t baseOffset_;
	std::vector<uint32_t> data_;
	size_t dataSize_;
};


#endif