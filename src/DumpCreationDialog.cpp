#include "DumpCreationDialog.hpp"
#include "ResultListWidgetItem.hpp"
#include "MemAnalyzer.hpp"
#include <QtWidgets>


DumpCreationDialog::DumpCreationDialog(ResultListWidgetItem* item, QWidget *parent) :
	QDialog(parent)
{
	setWindowTitle ("Create dump");
	setFixedSize(QSize(200, 150));

	QVBoxLayout* mainLayout = new QVBoxLayout(this);

	QHBoxLayout* prependLayout = new QHBoxLayout;
	mainLayout->addLayout(prependLayout);
	prependLayout->setAlignment(Qt::AlignLeft);
	QHBoxLayout* appendLayout = new QHBoxLayout;
	mainLayout->addLayout(appendLayout);
	appendLayout->setAlignment(Qt::AlignLeft);

	QLabel* prependLabel = new QLabel("Prepend: ");
	prependLayout->addWidget(prependLabel);
	prependLineEdit_ = new QLineEdit();
	prependLayout->addWidget(prependLineEdit_);
	prependLineEdit_->setText("100");		// delegate to futere Settings
	prependLineEdit_->setFixedSize(QSize(50, 30));
	prependLineEdit_->setValidator(new QRegExpValidator(QRegExp("\\d+"), this));

	QLabel* appendLabel = new QLabel("Append: ");
	appendLayout->addWidget(appendLabel);
	appendLineEdit_ = new QLineEdit();
	appendLayout->addWidget(appendLineEdit_);
	appendLineEdit_->setText("100");		// delegate to futere Settings
	appendLineEdit_->setFixedSize(QSize(50, 30));
	appendLineEdit_->setValidator(new QRegExpValidator(QRegExp("\\d+"), this));

	supportCheckBox_ = new QCheckBox("Dump analyzer support");
	mainLayout->addWidget(supportCheckBox_);
	supportCheckBox_->setCheckState(Qt::Checked);

	QHBoxLayout* buttonLayout = new QHBoxLayout; 
	mainLayout->addLayout(buttonLayout);
	buttonLayout->setAlignment(Qt::AlignCenter);

	dumpButton_ = new QPushButton("Create dump");
	buttonLayout->addWidget(dumpButton_);
	dumpButton_->setFixedSize(QSize(120, 30));
	connect(dumpButton_, SIGNAL(clicked()), this, SLOT(dumpData()));

	// to init list
	heapOffset_ = item->getHeapOffset();
	nameHint_ = item->getName();

}

// for now it supports only uint32_t
// should I add other types?
void DumpCreationDialog::dumpData()
{
	QString errorMsg;
	if(prependLineEdit_->text() == "")
	{
		errorMsg = QString("Set prepended size");
	} else if(appendLineEdit_->text() == "") {
		errorMsg = QString("Set appended size");
	}

	if(errorMsg != "")
	{
		QMessageBox msgBox;
		msgBox.setText(errorMsg);
		msgBox.exec();
		return;
	}

	size_t prependSize = prependLineEdit_->text().toLong();
	size_t appendSize = appendLineEdit_->text().toLong();

	if(!MemAnalyzer::getInstance().isReliableOffset(
		heapOffset_ + appendSize*sizeof(uint32_t)))
	{
		errorMsg = QString("Append size transcends heap");
	} else if(heapOffset_ < prependSize*sizeof(uint32_t)) {
		errorMsg = QString("Prepend size transcends heap");		
	}

	if(errorMsg != "")
	{
		QMessageBox msgBox;
		msgBox.setText(errorMsg);
		msgBox.exec();
		return;
	}

	if(!QDir("/tmp/peepslit_dumps").exists())
		QDir("/tmp").mkdir("peepslit_dumps");

	QString filename = QFileDialog::getSaveFileName(this,
		"Save File", "/tmp/peepslit_dumps/" + nameHint_, "Binary (*.bin)");

	MemAnalyzer::getInstance().dumpResult(filename.toStdString() + ".bin", heapOffset_,
		prependSize, appendSize, supportCheckBox_->checkState() == Qt::Checked);
	close();
}