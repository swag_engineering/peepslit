#include "ResultListWidgetItem.hpp"
#include "MemAnalyzer.hpp"
#include <QtWidgets>


ResultListWidgetItem::ResultListWidgetItem(QString offset, QString name, QListWidget* parent)
	: QListWidgetItem((name == "" ? offset : name + " : " + offset), parent),
	name_{name},
	heapOffset_{offset.toULong()}
{
	absPos_ = heapOffset_ + MemAnalyzer::getInstance().getHeapStart();
	
}