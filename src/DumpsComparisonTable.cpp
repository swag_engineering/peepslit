#include "DumpsComparisonTable.hpp"
#include <QtWidgets>
#include <limits>

#include <iostream>

DumpsComparisonTable::DumpsComparisonTable(QObject* parent)
	: QAbstractTableModel(parent),
	tableSize_{0},
	basePos_{0},
	aligned_{false}, 	//from future settings
	highlight_{false},	// also from future settings
	pathPrefix_{std::nullopt}
{

}

void DumpsComparisonTable::addFile(QString file)
{
	DumpStream stream = DumpStream(file);
	streams_.emplace(file, std::move(stream));
	tableSize_ = (stream.size() > tableSize_ ? stream.size() : tableSize_);
	header_.append(file);

	if(!pathPrefix_.has_value() || file.indexOf(pathPrefix_.value()) != 0)
		findCommonPathPrefix();

	size_t initRowsNum = 40;		// move to future settings
	if(!stream.isSupported())
	{
		notSupportedFiles_.append(file);
		if(aligned_)
		{
			breakAlignment();
			emit alignmentPossible(false);
		}
	} else if(aligned_) 
	{
		align();
	}
	if(streams_.size() == 1)
	{
		if(initRowsNum > tableSize_)
			initRowsNum = tableSize_;
		strData_ = collectData(basePos_, initRowsNum);
		return;
	}

	size_t size = strData_.size()/types_.size();
	if(size < initRowsNum && initRowsNum > tableSize_)
	{
		size = tableSize_;
	} else if(size < initRowsNum && initRowsNum < tableSize_)
	{
		size = initRowsNum;
	}
	strData_ = collectData(basePos_, size);
}

bool DumpsComparisonTable::removeColumn(int column, const QModelIndex& parent)
{
	QString file = header_[column];
	tableSize_ = 0;
	size_t dropOffset = streams_.find(file)->second.getFocusPos();
	size_t maxDistance = 0;
	streams_.erase(file);

	for(auto& [file, stream]: streams_)
	{
		stream.setOffset(0);
		tableSize_ = (stream.size() > tableSize_ ? stream.size() : tableSize_);
		size_t pos = stream.getFocusPos();
		maxDistance = (pos > maxDistance ? pos : maxDistance);
	}
	focusRow_ = maxDistance - basePos_;

	size_t deleteFromStart = 0;
	if(aligned_)
	{
		for(int i = 0; i < strData_.size(); i = i + types_.size())
		{
			bool exit = false;
			for(int c = 1; c < header_.size(); c++)
				if(!strData_[i][c].isEmpty() && c != column)
				{
					exit = true;
					break;
				}
			if(exit)
				break;
			removeRows(i, types_.size(), parent);
			deleteFromStart++;
		}
		for(auto& [file, stream]: streams_)
			stream.setOffset(maxDistance - stream.getFocusPos());
		if(maxDistance < dropOffset) {
			if(dropOffset - maxDistance > basePos_)
			{
				basePos_ = 0;
			} else {
				basePos_ -= dropOffset - maxDistance;
			}
		}
		focusRow_ = maxDistance - basePos_;
	}

	size_t deleteInTheEnd = 0;
	for(int i = strData_.size() - 1; i >= 0; i = i - types_.size())
	{
		bool exit = false;
		for(int c = 1; c < header_.size(); c++)
		{
			if(!strData_[i][c].isEmpty() && c != column)
			{
				exit = true;
				break;
			}
		}
		if(exit)
			break;
		removeRows(i - types_.size(), types_.size(), parent);
		deleteInTheEnd++;
	}
	header_.erase(header_.begin() + column);
	beginRemoveColumns(parent, column, column);
	strData_ = collectData(basePos_, strData_.size()/types_.size() -
		(deleteFromStart + deleteInTheEnd));
	endRemoveColumns();
	if(notSupportedFiles_.removeOne(file) && notSupportedFiles_.size() == 0)
		emit alignmentPossible(true);
	findCommonPathPrefix();
	return true;
}

QList<QStringList> DumpsComparisonTable::collectData(size_t pos, size_t count) {
	QList<QStringList> newList;
	for(size_t r=pos; r<pos+count; r++)
	{
		for(size_t sr=0; sr<(size_t)types_.size(); sr++)
		{
			QStringList rowList = {types_[sr]};
			for(size_t c=1; c<(size_t)header_.size(); c++)
			{
				DumpStream& stream = streams_.find(header_[c])->second;
				rowList.append((this->*typeToFuncMap_.at(types_[sr]))(stream[r]));
			}
			newList.append(rowList);
		}
	}
	return newList;
}

void DumpsComparisonTable::align()
{
	if(aligned_)
		return;

	size_t maxDistance = 0;
	for(const auto& [file, stream] : streams_)
	{
		assert(stream.isSupported());
		size_t pos = stream.getFocusPos();
		maxDistance = (pos > maxDistance ? pos : maxDistance);
	}
	for(auto& [file, stream] : streams_)
	{
		stream.setOffset(maxDistance - stream.getFocusPos());
		tableSize_ = (stream.size() > tableSize_ ? stream.size() : tableSize_);
	}

	size_t initRowsNum = 40;		// move to future settings
	if(maxDistance < initRowsNum/2)
	{
		initRowsNum -= initRowsNum/2 - maxDistance;
		basePos_ = 0;
		focusRow_ = maxDistance;
	} else {
		basePos_ = maxDistance - initRowsNum/2;
		focusRow_ = initRowsNum/2;
	}
	if(initRowsNum/2 + initRowsNum%2 > tableSize_ - basePos_)
	{
		initRowsNum -= tableSize_ - basePos_;
	}	
	if(initRowsNum > tableSize_)
		initRowsNum = tableSize_;
	beginResetModel();
	strData_ = collectData(basePos_, initRowsNum);
	aligned_ = true;
	// emit dataChanged(index(0, 0), index(strData_.size()-1, header_.size()-1));
	endResetModel();
}

void DumpsComparisonTable::breakAlignment()
{
	if(!aligned_)
		return;
	tableSize_ = 0;
	for(auto& [file, stream] : streams_)
	{
		stream.setOffset(0);
		tableSize_ = (stream.size() > tableSize_ ? stream.size() : tableSize_);
	}	
	size_t initRowsNum = 40;		// move to future settings
	basePos_ = 0;
	if(initRowsNum > tableSize_)
		initRowsNum = tableSize_;
	beginResetModel();
	strData_ = collectData(basePos_, initRowsNum);
	aligned_ = false;
	endResetModel();
	// emit dataChanged(index(0, 0), index(strData_.size()-1, header_.size()-1));
}


QVariant DumpsComparisonTable::headerData(int section, Qt::Orientation orientation,
	int role) const
{
	if (role == Qt::DisplayRole && orientation == Qt::Horizontal)
	{
		if(section == 0)
			return header_[section];
		QString headerName;
		if(pathPrefix_.has_value()) {
			headerName = QString(header_[section]).remove(0,
				pathPrefix_.value().size() + 1);
		} else {
			headerName = header_[section];
		}
		cutHeaderText(headerName);
		return headerName;
	} else if (role == Qt::ToolTipRole &&
		orientation == Qt::Horizontal &&
		section != 0)
	{
		return header_[section];
	}
	return QVariant();
}

void DumpsComparisonTable::cutHeaderText(QString& text) const
{
	// 130 - width of header
	// 20 - aproximate header height
	// move to future settings
	size_t freeSpace = (130 - 20)*0.9;
	size_t textSize = QFontMetrics(QApplication::font()).horizontalAdvance(text);
	if(textSize < freeSpace)
		return;
	size_t dotsSize = QFontMetrics(QApplication::font()).horizontalAdvance("...");
	for(;;)
	{
		text.chop(1);
		textSize = QFontMetrics(QApplication::font()).horizontalAdvance(text);
		if(textSize + dotsSize < freeSpace)
		{
			text.push_back("...");
			return;
		}
	}
}

void DumpsComparisonTable::findCommonPathPrefix()
{
	QList<QList<QDir>> dirs;
	QDir toDrop;
	size_t minSize = std::numeric_limits<size_t>::max();
	// spliting file paths into dirs untill root
	for(int idx = 1; idx < header_.size(); idx++)
	{
		QList<QDir> cur;
		QDir next = QDir(header_[idx]);
		next.cdUp();
		size_t cnt = 0;
		for(;;)
		{
			if(next.isRoot())
				break;
			cur.push_front(next);
			next.cdUp();
			cnt++;
		}
		minSize = (cnt < minSize ? cnt : minSize);
		dirs.append(cur);
	}
	size_t n;
	for(n = 0; n < minSize;)
	{
		bool same = true;
		for(int i = 1; i < dirs.size(); i++)
		{
			if(dirs[0][n] != dirs[i][n])
			{
				same = false;
				break;			
			}
		}
		if(!same)
			break;
		n++;
	}
	if(n == 0)
	{
		pathPrefix_ = std::nullopt;
	} else {
		pathPrefix_ = dirs[0][n-1].path();
	}
	emit headerDataChanged(Qt::Horizontal, 1, header_.size()-2);
}

int DumpsComparisonTable::rowCount(const QModelIndex&) const
{
	return strData_.size();
}

int DumpsComparisonTable::columnCount(const QModelIndex&) const
{
	return header_.size();
}

QVariant DumpsComparisonTable::data(const QModelIndex &index, int role) const
{
	if(!index.isValid() || role != Qt::DisplayRole)
		return QVariant();

	return strData_[index.row()][index.column()];
}

bool DumpsComparisonTable::isEmpty(const QModelIndex &index) const
{
	return strData_[index.row()][index.column()] == QString("");
}

QString DumpsComparisonTable::toHexString(std::optional<uint32_t> value) const
{
	return value.has_value() ? "0x" + QString::number(value.value(), 16) : QString();
}

QString DumpsComparisonTable::toChar(std::optional<uint32_t> value) const
{	
	if(!value.has_value())
		return QString();
	QString str;
	char *array = (char*)&(value.value());
	for(size_t i=0; i<sizeof(uint32_t); i++)
	{
		char ch = array[i];
		str += (ch < 33 || ch > 126) ? QChar(0x25AF) : ch;
		str += " ";
	}
	return str;
}

size_t DumpsComparisonTable::getColumnWidth(size_t idx) const
{
	return (idx ? 130 : 60);
}

void DumpsComparisonTable::setHighlight(bool highlight)
{
	if(highlight != highlight_) {
		highlight_ = highlight;
		emit dataChanged(index(0, 0), index(strData_.size()-1, header_.size()-1));
	}
}

std::optional<bool> DumpsComparisonTable::rowIsEqual(size_t row) const
{
	bool state = true;
	size_t cnt = 0;
	row -= row%types_.size();
	for(size_t i=1; i<(size_t)header_.size(); i++)
	{
		if(strData_[row][i] == QString(""))
			continue;
		if(strData_[row][1] != strData_[row][i])
			state = false;
		cnt++;
	}
	if(cnt < 2)
		return std::nullopt;
	return state;

}

size_t DumpsComparisonTable::insertAtStart(size_t count)
{
	if(!(bool)basePos_)
		return 0;

	count = (basePos_ > count ? count : basePos_);
	size_t last = count*types_.size() - 1;
	beginInsertRows(QModelIndex(), 0, last);
	basePos_ -= count;
	QList<QStringList> newList = collectData(basePos_, count);
	strData_ = newList + strData_;
	focusRow_ += count;
	endInsertRows();
	return count;
}

size_t DumpsComparisonTable::insertInTheEnd(size_t count)
{
	size_t rest = tableSize_ - (basePos_ + strData_.size() / types_.size());
	if(!(bool)rest)
		return 0;
	count = (rest > count ? count : rest);
	size_t first = strData_.size();
	size_t last = first + +count*types_.size() - 1;
	beginInsertRows(QModelIndex(), first, last);
	QList<QStringList> newList = collectData(basePos_ +
		strData_.size() / types_.size(), count);
	strData_ += newList;
	endInsertRows();
	return count;
}