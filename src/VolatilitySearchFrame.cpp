#include "VolatilitySearchFrame.hpp"
#include "MemAnalyzer.hpp"


VolatilitySearchFrame::VolatilitySearchFrame(QWidget* parent)
	: ISearchFrame(parent),
	searchThread_{nullptr}
{
	QVBoxLayout* mainLayout = new QVBoxLayout(this);

	QHBoxLayout* splitLayout = new QHBoxLayout();
	mainLayout->addLayout(splitLayout);

	modeGroupBox_ = new QGroupBox("Search types:");
	splitLayout->addWidget(modeGroupBox_);

	QVBoxLayout* groupBoxLayout = new QVBoxLayout();
	modeGroupBox_->setLayout(groupBoxLayout);

	volatileRadioButton_ = new QRadioButton("volatile");
	volatileRadioButton_->setChecked(true);
	groupBoxLayout->addWidget(volatileRadioButton_);
	nonVolatileRadioButton_ = new QRadioButton("non-volatile");
	groupBoxLayout->addWidget(nonVolatileRadioButton_);

	QVBoxLayout* managmentLayout = new QVBoxLayout();
	splitLayout->addLayout(managmentLayout);

	QHBoxLayout* countLayout = new QHBoxLayout();
	managmentLayout->addLayout(countLayout);

	QLabel* countLabel = new QLabel("Count: ");
	countLayout->addWidget(countLabel);

	countLineEdit_ = new QLineEdit(QString::number(0));
	countLineEdit_->setFixedSize(QSize(50, 30));
	countLineEdit_->setReadOnly(true);
	countLayout->addWidget(countLineEdit_);

	QHBoxLayout* delayLayout = new QHBoxLayout();
	managmentLayout->addLayout(delayLayout);

	QLabel* delayLabel = new QLabel("Delay: ");
	delayLayout->addWidget(delayLabel);

	delaySpinBox_ = new QSpinBox();
	delayLayout->addWidget(delaySpinBox_);
	delaySpinBox_->setSuffix("ms");
	delaySpinBox_->setRange(10, 100);
	delaySpinBox_->setSingleStep(10);
	delaySpinBox_->setValue(10);

	QHBoxLayout* buttonsLayout = new QHBoxLayout();
	mainLayout->addLayout(buttonsLayout);

	findButton_ = new QPushButton("Start");
	findButton_->setFixedSize(QSize(50, 30));
	buttonsLayout->addWidget(findButton_);
	connect(findButton_, SIGNAL(clicked()), this, SLOT(findResults()));

	resetButton_ = new QPushButton("Reset");
	resetButton_->setFixedSize(QSize(50, 30));
	buttonsLayout->addWidget(resetButton_);
	connect(resetButton_, SIGNAL(clicked()), this, SLOT(resetResults()));

	resultsLable_ = new QLabel("Results found: 0");
	mainLayout->addWidget(resultsLable_);

	qRegisterMetaType<size_t>("size_t");
	connect(this, SIGNAL(cntChanged(size_t, size_t)), this, SLOT(onCntChanged(size_t, size_t)));

	resetResults();
}

VolatilitySearchFrame::~VolatilitySearchFrame()
{
	if(searchThread_)
	{
		searchThread_->requestInterruption();
		searchThread_->wait();
		delete searchThread_;
	}
	resetResults();
}

void VolatilitySearchFrame::findResults()
{
	static bool searchRunning = false;
	if(searchRunning)
	{
		searchRunning = !searchRunning;
		findButton_->setEnabled(false);
		searchThread_->requestInterruption();
		searchThread_->wait();
		delete searchThread_;
		searchThread_ = nullptr;

		findButton_->setEnabled(true);
		resetButton_->setEnabled(true);
		delaySpinBox_->setEnabled(true);
		modeGroupBox_->setEnabled(true);
		findButton_->setText("Start");
	} else {
		searchRunning = !searchRunning;
		findButton_->setText("Stop");
		modeGroupBox_->setEnabled(false);
		delaySpinBox_->setEnabled(false);
		resetButton_->setEnabled(false);

		searchThread_ = QThread::create(
			std::bind(&VolatilitySearchFrame::searchLoop, this,
				volatileRadioButton_->isChecked()), this);
		searchThread_->start();
	}
}

void VolatilitySearchFrame::resetResults()
{
	MemAnalyzer::getInstance().reset();
	emit resultsUpdated();
	resultsLable_->setText("Results found: " +
		QString::number(MemAnalyzer::getInstance().getResults().size()));
	emit cntChanged(0, 0);
	findButton_->setEnabled(true);
}

void VolatilitySearchFrame::searchLoop(bool volatileSearch)
{
	size_t resultsSize = 0;
	size_t cnt = 0;
	emit cntChanged(cnt, resultsSize);
	while(true)
	{
		if(QThread::currentThread()->isInterruptionRequested())
			return;
		QThread::msleep(delaySpinBox_->value());
		MemAnalyzer::getInstance().findVolatile(volatileSearch, !(bool)cnt);
		resultsSize = MemAnalyzer::getInstance().getResults().size();
		resultsLable_->setText("Results found: " + QString::number(resultsSize));
		emit cntChanged(cnt++, resultsSize);		// postincrement, since first itereation is for initialization of the buffers
		// if(resultsSize < 1000)		// should be moved to future Settings
		emit resultsUpdated();
		if(resultsSize == 0 && cnt > 2)
			return;
	}
}

void VolatilitySearchFrame::onCntChanged(size_t cnt, size_t resultsSize)
{
	countLineEdit_->setText(QString::number(cnt));
	if(resultsSize == 0 && cnt > 1)
	{
		findResults();
		findButton_->setEnabled(false);
	}
}