#ifndef PEEPSLIT_MEM_INSPECTOR_DELEGATE_H
#define PEEPSLIT_MEM_INSPECTOR_DELEGATE_H

#include <QStyledItemDelegate>
#include <QSize>
#include "MemInspectorTable.hpp"

// create source file??
class MemInspectorDelegate : public QStyledItemDelegate
{
	Q_OBJECT
public:
	MemInspectorDelegate(QObject* parent = 0)
		: QStyledItemDelegate(parent) {};
	
	void initStyleOption(QStyleOptionViewItem *option,
		const QModelIndex &index) const
	{
		QStyledItemDelegate::initStyleOption(option, index);
		const MemInspectorTable* model = \
			dynamic_cast<const MemInspectorTable*>(index.model());
		if((size_t)index.row() == model->getFocusRow())
		{
			option->backgroundBrush = QBrush("#69b7ff");
		} else if((index.row() - (int)model->getFocusRow()) % 10 == 0) {
			option->backgroundBrush = QBrush("#7df08c");
		}
		if((size_t)index.column() == model->getPointerColumnIdx() &&
				model->addrInHeapRange(model->getQStringValue(index)))
			option->backgroundBrush = QBrush("#ffc2d4");
	}

	QSize sizeHint( const QStyleOptionViewItem& option,
		const QModelIndex& index ) const
	{
		const MemInspectorTable* model = \
			dynamic_cast<const MemInspectorTable*>(index.model());
		return QSize(model->getColumnWidth((size_t)index.column()), option.rect.height());
	}
};

#endif
