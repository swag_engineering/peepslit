#ifndef PEEPSLIT_IN_RANGE_SEARCH_FRAME_H
#define PEEPSLIT_IN_RANGE_SEARCH_FRAME_H

#include "ISearchFrame.hpp"

class QLineEdit;
class QPushButton;
class QComboBox;
class QLabel;

class InRangeSearchFrame : public ISearchFrame
{
	Q_OBJECT
public:
	InRangeSearchFrame();

public slots:
	void findResults() override;
	void resetResults() override;

private:	
	QComboBox* typeSelector_;
	QLineEdit* leftLimit_;
	QLineEdit* rightLimit_;
	QPushButton* findButton_;
	QLabel* resultsLable_;

	std::vector<std::string> typesMap_ = {"uint32_t", "float", "uint64_t"};
};

#endif