#include "MainWindow.hpp"
#include "SearchWindow.hpp"
#include "ResultListWidgetItem.hpp"
#include "MemAnalyzer.hpp"
#include "DumpCreationDialog.hpp"
#include "DumpsComparisonWindow.hpp"
#include "MemInspectorWindow.hpp"
#include <regex>

MainWindow::MainWindow(QWidget *parent) : QWidget(parent)
{
	setWindowTitle("Peepslit");
	setFixedSize(QSize(380, 340));

	QHBoxLayout* mainLayout = new QHBoxLayout(this);

	QVBoxLayout* managmentLayout = new QVBoxLayout;
	mainLayout->addLayout(managmentLayout);

	QVBoxLayout* resultsLayout = new QVBoxLayout;
	mainLayout->addLayout(resultsLayout);
	resultsLayout->setContentsMargins(40, 0, 0, 0);


	QHBoxLayout* processLayout = new QHBoxLayout;
	managmentLayout->addLayout(processLayout);

	processLineEdit_ = new QLineEdit;
	processLayout->addWidget(processLineEdit_);
	processLineEdit_->setFixedSize(QSize(120, 30));
	processLineEdit_->setPlaceholderText("Name or PID");
	connect(processLineEdit_, SIGNAL(returnPressed()), this, SLOT(findProcess()));
	// processLineEdit_->setValidator(new QRegExpValidator(QRegExp("[0-9a-zA-z_]+"), this));

	processButton_ = new QPushButton("Find");
	processLayout->addWidget(processButton_);
	processButton_->setFixedSize(QSize(60, 30));
	connect(processButton_, SIGNAL(clicked()), this, SLOT(findProcess()));

	nameInfo_ = new QLabel;
	managmentLayout->addWidget(nameInfo_);

	pidInfo_ = new QLabel;
	managmentLayout->addWidget(pidInfo_);

	heapAddrInfo_ = new QLabel;
	std::stringstream hexStream;
	hexStream << std::hex << MemAnalyzer::getInstance().getHeapStart();
	managmentLayout->addWidget(heapAddrInfo_);

	heapLenInfo_ = new QLabel;
	managmentLayout->addWidget(heapLenInfo_);

	clearInfo();

	observeButton_ = new QPushButton("Observe heap");
	managmentLayout->addWidget(observeButton_);
	observeButton_->setFixedSize(QSize(120, 30));
	observeButton_->setEnabled(false);
	connect(observeButton_, SIGNAL(clicked()), this, SLOT(openMemInspector()));

	findButton_ = new QPushButton("Search in heap");
	managmentLayout->addWidget(findButton_);
	findButton_->setFixedSize(QSize(120, 30));
	findButton_->setEnabled(false);
	connect(findButton_, SIGNAL(clicked()), this, SLOT(openFindWindow()));

	compareDumpsButton_ = new QPushButton("Compare dumps");
	managmentLayout->addWidget(compareDumpsButton_);
	compareDumpsButton_->setFixedSize(QSize(120, 30));
	connect(compareDumpsButton_, SIGNAL(clicked()), this, SLOT(compareDumps()));

	managmentLayout->addStretch();

	QHBoxLayout* labelLayout = new QHBoxLayout;
	resultsLayout->addLayout(labelLayout);
	labelLayout->setAlignment(Qt::AlignCenter);
	QLabel* resultsLable = new QLabel("Saved results:");
	labelLayout->addWidget(resultsLable);
	// mainLayout->addStretch(1);

	resultsList_ = new QListWidget;
	resultsLayout->addWidget(resultsList_, 0, Qt::AlignLeft);
	resultsList_->setMaximumSize(150, 300);
	resultsList_->setContextMenuPolicy(Qt::CustomContextMenu);
	resultsList_->setSelectionMode(QAbstractItemView::SingleSelection);
	connect(resultsList_, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showContextMenu(QPoint)));

    QShortcut* shortcut = new QShortcut(QKeySequence(Qt::Key_Delete), resultsList_);
    connect(shortcut, &QShortcut::activated, this, [=]()
		{
			delete resultsList_->selectedItems()[0]; // multiple selection is forbiden
		});
}


void updateSelection(size_t maxSelections, QItemSelectionModel *sm,
	const QItemSelection&,	const QItemSelection&)
{
	static QItemSelection last_selected;

	static bool recursing = false;
	if (recursing)
		return;
	if ((size_t)sm->selectedRows().size() > maxSelections) {
		recursing = true;
		sm->clearSelection();
		for (const auto &i: last_selected.indexes()) {
			sm->select(i, QItemSelectionModel::Select);
		}
		recursing = false;
	}
	last_selected = sm->selection();
}


void MainWindow::compareDumps()
{
	QString startDir = "/tmp/peepslit_dumps";
	if(!QDir(startDir).exists())
		startDir = "/tmp";

	dialog = new QFileDialog(this, "Select one or more files to open", startDir,
		"Binary (*.bin);;All files (*.*)");
	dialog->setFileMode(QFileDialog::ExistingFiles);

	// shameless workaround from stackoverflow
	for (const auto &i: dialog->findChildren<QListView *>("listView")) {
		QItemSelectionModel* sm = i->selectionModel();
		connect(sm, &QItemSelectionModel::selectionChanged,
		[sm](const QItemSelection &selected, const QItemSelection &deselected)
		 {
		   updateSelection(5, sm, selected, deselected);	// move to future settings
		 });
	}

	dialog->exec();
	QStringList files = dialog->selectedFiles();
	if(files.size() != 0)
	{		
		DumpsComparisonWindow* window = new DumpsComparisonWindow(files, this);
		window->setAttribute(Qt::WA_DeleteOnClose);
		connect(window, &DumpsComparisonWindow::finished, 
			[=](int) { compareDumpsButton_->setEnabled(true); });
		compareDumpsButton_->setEnabled(false);
		window->show();
	}
}

void MainWindow::updateInfo()
{
	MemAnalyzer::getInstance().updateInfo();

	nameInfo_->setText("Name: " + \
		QString(MemAnalyzer::getInstance().getName().c_str()));

	pidInfo_->setText("Pid: " + \
		QString::number(MemAnalyzer::getInstance().getPid()));

	std::stringstream hexStream;
	hexStream << std::hex << MemAnalyzer::getInstance().getHeapStart();
	heapAddrInfo_->setText("Heap start: 0x" + QString(hexStream.str().c_str()));

	size_t heapSize = MemAnalyzer::getInstance().getHeapLen();
	if(heapSize > pow(10, 9)) {
		heapLenInfo_->setText("Heap length: " + \
			QString::number(heapSize/pow(10, 9), 'g', 3) + "Gb");
	} else if(heapSize > pow(10, 6)) {		
		heapLenInfo_->setText("Heap length: " + \
			QString::number(heapSize/pow(10, 6), 'g', 3) + "Mb");
	} else {		
		heapLenInfo_->setText("Heap length: " + \
			QString::number(heapSize/pow(10, 3), 'g', 3) + "Kb");
	}
}

void MainWindow::clearInfo()
{
	nameInfo_->setText("Name:");
	pidInfo_->setText("Pid:");
	heapAddrInfo_->setText("Heap start:");
	heapLenInfo_->setText("Heap length:");
}

void MainWindow::openFindWindow()
{
	SearchWindow* searchWindow_ = new SearchWindow;
	searchWindow_->setAttribute(Qt::WA_DeleteOnClose);	
	connect(searchWindow_, &SearchWindow::saveResult,  
		[=](ResultListWidgetItem* item) { resultsList_->addItem(item); });
	connect(searchWindow_, &SearchWindow::finished, 
		[=](int) { findButton_->setEnabled(true); });
	findButton_->setEnabled(false);
	searchWindow_->show();
}

void MainWindow::findProcess()
{
	try
	{
		MemAnalyzer::getInstance().init(processLineEdit_->text().toStdString());
		updateInfo();
		findButton_->setEnabled(true);
		observeButton_->setEnabled(true);
	}
	catch(const std::runtime_error& ex)
	{
		clearInfo();		
		findButton_->setEnabled(false);
		observeButton_->setEnabled(false);
		QMessageBox msgBox;
		msgBox.setWindowTitle("Error");
		msgBox.setText(ex.what());
		msgBox.exec();
		return;
	}
}

void MainWindow::showContextMenu(const QPoint& pos)
{
	QPoint globalPos = resultsList_->mapToGlobal(pos);

	QMenu myMenu;
	myMenu.addAction("Dump", this, SLOT(dumpResults()));
	myMenu.addAction("Delete",  this, [=]()
		{
			delete resultsList_->selectedItems()[0]; // multiple selection is forbiden
		});

	myMenu.exec(globalPos);
}


void MainWindow::dumpResults()
{
	DumpCreationDialog* dialog = new DumpCreationDialog(
		dynamic_cast<ResultListWidgetItem*>(resultsList_->selectedItems()[0]),
		this);
	dialog->setAttribute(Qt::WA_DeleteOnClose);
	dialog->show();
}

void MainWindow::openMemInspector()
{
	MemInspectorWindow* inspectorWindow = new MemInspectorWindow(
		MemAnalyzer::getInstance().getHeapStart(), this);	
	inspectorWindow->setAttribute(Qt::WA_DeleteOnClose);
	inspectorWindow->show();
	connect(inspectorWindow, &MemInspectorWindow::finished, 
			[=](int) { observeButton_->setEnabled(true); });
	observeButton_->setEnabled(false);
}