#ifndef MEM_RESULT_LIST_WIDGET_ITEM_H
#define MEM_RESULT_LIST_WIDGET_ITEM_H

#include <QListWidgetItem>
#include <cstddef>

// class QListWidgetItem;

class ResultListWidgetItem : public QListWidgetItem
{
public:
	ResultListWidgetItem(QString offset, QString name,
		QListWidget* parent=nullptr);
	size_t getAbsPosition() { return absPos_; };
	size_t getHeapOffset() { return heapOffset_; };
	QString getName() { return name_; };

private:
	QString name_;
	size_t absPos_;
	size_t heapOffset_;
};

#endif