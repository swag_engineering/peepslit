#ifndef PEEPSLIT_DUMPS_COMPARISON_WINDOW_H
#define PEEPSLIT_DUMPS_COMPARISON_WINDOW_H

#include <QDialog>

class QCheckBox;
class QString;
class QStringList;
class QTableView;
class QScrollBar;
class DumpsComparisonTable;

class DumpsComparisonWindow : public QDialog
{
	Q_OBJECT
public:
	DumpsComparisonWindow(QStringList files, QWidget* parent=nullptr);

private:
	void updateWidth();
	void showEvent(QShowEvent* event);
	void changeSliderPos(float proc);
	void hide(size_t start, size_t len);
	void showAll();

	QCheckBox* alignmentCheckBox_;
	QCheckBox* highlightCheckBox_;
	QTableView* tableView_;
	DumpsComparisonTable* table_;
	QScrollBar* slider_;


public slots:
	void sliderChanged(int value);
	void alignmentChanged(int value);
	void highlightChanged(int value);
	void onTableClicked(const QModelIndex &index);
	void onAlignmentStatusChanged(bool status);
};


#endif