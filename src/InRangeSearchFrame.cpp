#include "InRangeSearchFrame.hpp"
#include "MemAnalyzer.hpp"
#include <QtWidgets>


InRangeSearchFrame::InRangeSearchFrame()
{

	QVBoxLayout* mainLayout = new QVBoxLayout(this);

	QHBoxLayout* parametersLayout = new QHBoxLayout;
	mainLayout->addLayout(parametersLayout);

	typeSelector_ = new QComboBox;
	typeSelector_->setFixedSize(QSize(100, 30));
	parametersLayout->addWidget(typeSelector_);
	for (auto const& element : typesMap_)
    	typeSelector_->addItem(element.c_str());

	leftLimit_ = new QLineEdit;
	leftLimit_->setFixedSize(QSize(50, 30));
	leftLimit_->setPlaceholderText("From");
	leftLimit_->setValidator(new QRegExpValidator(QRegExp("\\d*"), this));
	parametersLayout->addWidget(leftLimit_);

	rightLimit_ = new QLineEdit;
	rightLimit_->setFixedSize(QSize(50, 30));
	rightLimit_->setPlaceholderText("To");
	rightLimit_->setValidator(new QRegExpValidator(QRegExp("\\d*"), this));
	parametersLayout->addWidget(rightLimit_);

	QHBoxLayout* buttonsLayout = new QHBoxLayout;
	mainLayout->addLayout(buttonsLayout);

	findButton_ = new QPushButton("Find");
	findButton_->setFixedSize(QSize(50, 30));
	buttonsLayout->addWidget(findButton_);
	connect(findButton_, SIGNAL(clicked()), this, SLOT(findResults()));

	QPushButton* resetButton = new QPushButton("Reset");
	resetButton->setFixedSize(QSize(50, 30));
	buttonsLayout->addWidget(resetButton);
	connect(resetButton, SIGNAL(clicked()), this, SLOT(resetResults()));

	resultsLable_ = new QLabel("Results found: 0");
	resultsLable_->setAlignment(Qt::AlignBottom);
	mainLayout->addWidget(resultsLable_);
}

void InRangeSearchFrame::findResults()
{
	QString type = typeSelector_->currentText();
	if("" == leftLimit_->text() || "" == rightLimit_->text())
	{
		QMessageBox msgBox;
		msgBox.setText("Write values to search");
		msgBox.exec();
		return;
	}
	if("uint32_t" == type) {
		MemAnalyzer::getInstance().findInRange(static_cast<uint32_t>(leftLimit_->text().toInt()),
			static_cast<uint32_t>(leftLimit_->text().toInt()));
	} else if("float" == type) {
		MemAnalyzer::getInstance().findInRange(static_cast<float>(leftLimit_->text().toInt()),
			static_cast<float>(leftLimit_->text().toInt()));
	} else if("uint64_t" == type) {
		MemAnalyzer::getInstance().findInRange(static_cast<uint64_t>(leftLimit_->text().toInt()),
			static_cast<uint64_t>(leftLimit_->text().toInt()));
	}

	size_t size = MemAnalyzer::getInstance().getResultsSize();
	resultsLable_->setText("Results found: " + QString::number(size));
	if(size == 0)
		findButton_->setEnabled(false);
	emit resultsUpdated();
}

void InRangeSearchFrame::resetResults()
{
	MemAnalyzer::getInstance().reset();
	resultsLable_->setText("Results found: 0");
	findButton_->setEnabled(true);
	emit resultsUpdated();
}