#ifndef PEEPSLIT_VOLATILE_SEARCH_FRAME_H
#define PEEPSLIT_VOLATILE_SEARCH_FRAME_H

#include "ISearchFrame.hpp"
#include <QtWidgets>

class VolatilitySearchFrame : public ISearchFrame {
	Q_OBJECT
public:
	VolatilitySearchFrame(QWidget* parent=nullptr);
	~VolatilitySearchFrame();

public slots:
	void findResults() override;
	void resetResults() override;
	void onCntChanged(size_t cnt, size_t resultsSize);

signals:
	void cntChanged(size_t cnt, size_t resultsSize);

private:
	QLabel* resultsLable_;
	QSpinBox* delaySpinBox_;
	QGroupBox* modeGroupBox_;
	QRadioButton* volatileRadioButton_;
	QRadioButton* nonVolatileRadioButton_;
	QPushButton* findButton_;
	QPushButton* resetButton_;
	QLabel* countLabel_;
	QLineEdit* countLineEdit_;
	QThread* searchThread_ = nullptr;

	void searchLoop(bool volatileSearch);
};

#endif