#include <vector>
#include <fstream>
#include <sys/ptrace.h>
#include <sys/wait.h>
#include <sys/types.h>


class MemAnalyzer
{
public:
	// move both to future settings
	const uint32_t anyNumber = 0xaabbccdd;
	const uint64_t dumpPrefix = 0x0000aabbccddeeff;

	static MemAnalyzer& getInstance();
	MemAnalyzer(const MemAnalyzer&) = delete;
    MemAnalyzer& operator=(const MemAnalyzer&) = delete;

	void init(const std::string pidName);
    void updateInfo();
	std::string getName() const { return name_; };
	size_t getPid() const { return pid_; };
	size_t getHeapStart() const { return heapStart_; };
	size_t getHeapLen() const { return heapLen_; };
	bool isReliableOffset(size_t offset) const { return offset < heapLen_; };
	std::vector<size_t>& getResults() { return results_; };
	size_t getResultsSize() const { return results_.size(); };
	void reset();
	void dumpResult(std::string filename, size_t offset,
		size_t left, size_t right, bool support);

	template <typename T>
	void readMem(T* ptr, size_t start, size_t len);
	template <typename T>
	void readMem(T* ptr, size_t start);
	template <typename T>
	void findExact(T needle);
	template <typename T>
	void findInRange(T left, T right);
	template <typename T>
	void findSequence(std::vector<T> needle);

	void findVolatile(bool volatile, bool initValues);

private:
	std::string name_;
	pid_t pid_;
	size_t heapStart_;
	size_t heapLen_;
	std::ifstream memSream_;
	uint8_t* memBuffer_;
	std::vector<size_t> results_;	// holds offsets
	uint32_t* saveBuffer_;
	bool initialized_;

	MemAnalyzer();
	~MemAnalyzer();

	void findPid();
	void collectMemoryRange();
	void allocateBuffer();
	void deallocateBuffer();
	void fillBuffer();
	void attachToMem();
	void deattachFromMem();
	
	
};

inline void MemAnalyzer::attachToMem()
{
	ptrace(PTRACE_ATTACH, pid_, NULL, NULL);
	waitpid(pid_, NULL, 0);	
}

inline void MemAnalyzer::deattachFromMem()
{
	ptrace(PTRACE_DETACH, pid_, NULL, NULL);
}

template <typename T>
void MemAnalyzer::readMem(T* ptr, size_t start, size_t len)
{
	attachToMem();
	memSream_.seekg(start);
	memSream_.read((char*)ptr, len*sizeof(T));
	deattachFromMem();
}

template <typename T>
void MemAnalyzer::readMem(T* ptr, size_t start)
{
	readMem(ptr, start, 1);
}

template <typename T>
void MemAnalyzer::findExact(T needle)
{
	if(!initialized_)
	{
		fillBuffer();
		T* haystack = reinterpret_cast<T*>(memBuffer_);
		size_t size = heapLen_/sizeof(T);
		for(size_t n=0; n < size; n++)
			if(haystack[n] == needle)
				results_.push_back(n*sizeof(T));
		initialized_ = true;
	} else
	{
		T value;
		for (size_t n=0; n < results_.size();)
		{
			readMem(&value, heapStart_ + results_[n]);
			if(value != needle)
			{
				results_.erase(results_.begin() + n);
				continue;
			}
			n++;
		}
	}
}

template <typename T>
void MemAnalyzer::findInRange(T left, T right)
{
	if(!initialized_)
	{
		fillBuffer();
		T* haystack = reinterpret_cast<T*>(memBuffer_);
		size_t size = heapLen_/sizeof(T);
		for(size_t n=0; n < size; n++)
			if(haystack[n] >= left && haystack[n] <= right)
				results_.push_back(n*sizeof(T));
		initialized_ = true;
	} else
	{
		T value;
		for (size_t n=0; n < results_.size();)
		{
			readMem(&value, heapStart_ + results_[n]);
			if(value < left || value > right)
			{
				results_.erase(results_.begin() + n);
				continue;
			}
			n++;
		}
	}
}


template <typename T>
void MemAnalyzer::findSequence(std::vector<T> needle)
{
	fillBuffer();
	T* haystack = reinterpret_cast<T*>(memBuffer_);
	for(size_t i=0; i<heapLen_/sizeof(T); i++)
	{
		bool found = true;
		for(size_t n=0; n<needle.size(); n++)
		{
			if(needle[n] == anyNumber)
				continue;
			if(haystack[i+n] != needle[n])
			{
				found = false;
				break;
			}
		}
		if(found)
			results_.push_back(i*sizeof(T));
	}
}
