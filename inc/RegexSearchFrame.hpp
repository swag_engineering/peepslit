#ifndef PEEPSLIT_REGEX_SEARCH_FRAME_H
#define PEEPSLIT_REGEX_SEARCH_FRAME_H

#include "ISearchFrame.hpp"

class QTextEdit;
class QLabel;
class QPushButton;

class RegexSearchFrame : public ISearchFrame {
	Q_OBJECT
public:
	RegexSearchFrame(QWidget* parent=nullptr);

public slots:
	void findResults() override;
	void resetResults() override;

private:
	QTextEdit* searchSeq_;
	QLabel* resultsLable_;
	QPushButton* findButton_;

};

#endif