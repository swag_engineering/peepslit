#include "DumpsComparisonDelegate.hpp"
#include "DumpsComparisonTable.hpp"
#include <optional>


// 1) aligned + highlight -> red + green(and extra green/red on focus) 
// 		and common colors when only one value exists
// 2) not aligned + highlight -> red + green
// 		and common colors when only one value exists
// 3) not aligned/aligned + no highlight -> normal color for all rows

	
void DumpsComparisonDelegate::initStyleOption(QStyleOptionViewItem *option,
	const QModelIndex &index) const
{
	QStyledItemDelegate::initStyleOption(option, index);
	const DumpsComparisonTable* model = \
		dynamic_cast<const DumpsComparisonTable*>(index.model());

	size_t aliquot = model->typesNumber();
	// 0 column is type one, should it be changed to avoid magic numbers?
	if(model->highlighted() && !model->isEmpty(index) && index.column() != 0)
	{
		std::optional<bool> equal = model->rowIsEqual(index.row());
		if(model->aligned() && index.column() != 0 &&
			(size_t)index.row()/aliquot == model->focusRow()/aliquot)
		{
			if(equal.has_value())
			{
				if(equal.value()) {
					option->backgroundBrush = QBrush(focusRight);
				} else {
					option->backgroundBrush = QBrush(focusWrong);
				}
				return;
			}
		}
		if(equal.has_value())
		{
			if(equal.value()) {
				if((size_t)index.row()%(aliquot*2) < aliquot)
				{
					option->backgroundBrush = QBrush(alternateRightOne);
				} else {
					option->backgroundBrush = QBrush(alternateRightTwo);		
				}
			} else {
				if((size_t)index.row()%(aliquot*2) < aliquot)
				{
					option->backgroundBrush = QBrush(alternateWrongOne);
				} else {
					option->backgroundBrush = QBrush(alternateWrongTwo);		
				}
			}
			return;
		}
	}

	if((size_t)index.row()%(aliquot*2) < aliquot)
	{
		option->backgroundBrush = QBrush(alternateOne);
	} else {
		option->backgroundBrush = QBrush(alternateTwo);		
	}
	if(model->aligned() && index.column() != 0 &&
		(size_t)index.row()/aliquot == model->focusRow()/aliquot)
	{
		option->backgroundBrush = QBrush(focus);		
	}

	// if((size_t)index.row() == model->getFocusRow())
	// {
	// 	option->backgroundBrush = QBrush("#69b7ff");
	// } else if(index.row()%10 == 0) {
	// 	option->backgroundBrush = QBrush("#7df08c");
	// }
	// if((size_t)index.column() == model->getPointerColumnIdx() &&
	// 		model->addrInHeapRange(model->getQStringValue(index)))
	// 	option->backgroundBrush = QBrush("#ffc2d4");
}

QSize DumpsComparisonDelegate::sizeHint(const QStyleOptionViewItem& option,
	const QModelIndex& index) const
{
	const DumpsComparisonTable* model = \
		dynamic_cast<const DumpsComparisonTable*>(index.model());
	return QSize(model->getColumnWidth((size_t)index.column()), option.rect.height());
}
