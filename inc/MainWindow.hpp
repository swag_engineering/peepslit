#ifndef PEEPSLIT_MAIN_WINDOW_H
#define PEEPSLIT_MAIN_WINDOW_H

#include <QtWidgets>

class ResultListWidgetItem;

class MainWindow : public QWidget {
	Q_OBJECT
public:
	MainWindow(QWidget *parent = nullptr);

private:

	void clearInfo();

	QLabel* nameInfo_;
	QLabel* pidInfo_;
	QLabel* heapAddrInfo_;
	QLabel* heapLenInfo_;
	QPushButton* observeButton_;
	QPushButton* findButton_;
	QPushButton* compareDumpsButton_;
	QListWidget* resultsList_;
	QLineEdit* processLineEdit_;
	QPushButton* processButton_;
	QFileDialog* dialog;

public slots:
	void openFindWindow();
	void updateInfo();
	void findProcess();
	void showContextMenu(const QPoint& pos);
	void dumpResults();
	void compareDumps();
	void openMemInspector();
};

#endif