#include "ExactSearchFrame.hpp"
#include "MemAnalyzer.hpp"
#include <QtWidgets>


ExactSearchFrame::ExactSearchFrame(QWidget* parent)
	: ISearchFrame(parent)
{
	QVBoxLayout* mainLayout = new QVBoxLayout(this);

	QHBoxLayout* parametersLayout = new QHBoxLayout;
	mainLayout->addLayout(parametersLayout);

	typeSelector_ = new QComboBox;
	typeSelector_->setFixedSize(QSize(100, 30));
	parametersLayout->addWidget(typeSelector_);
	for (auto const& element : _typesMap)
		typeSelector_->addItem(element.c_str());

	searchValue_ = new QLineEdit;
	searchValue_->setFixedSize(QSize(50, 30));
	searchValue_->setPlaceholderText("Value");
	searchValue_->setValidator(new QRegExpValidator(QRegExp("\\d*"), this));
	parametersLayout->addWidget(searchValue_);

	QHBoxLayout* buttonsLayout = new QHBoxLayout;
	mainLayout->addLayout(buttonsLayout);

	findButton_ = new QPushButton("Find");
	findButton_->setFixedSize(QSize(50, 30));
	buttonsLayout->addWidget(findButton_);	
	connect(findButton_, SIGNAL(clicked()), this, SLOT(findResults()));

	QPushButton* resetButton = new QPushButton("Reset");
	resetButton->setFixedSize(QSize(50, 30));
	buttonsLayout->addWidget(resetButton);
	connect(resetButton, SIGNAL(clicked()), this, SLOT(resetResults()));

	resultsLable_ = new QLabel("Results found: 0");
	resultsLable_->setAlignment(Qt::AlignBottom);
	mainLayout->addWidget(resultsLable_);
}

void ExactSearchFrame::findResults()
{
	QString type = typeSelector_->currentText();
	if("" == searchValue_->text())
	{
		QMessageBox msgBox;
		msgBox.setText("Write value to search");
		msgBox.exec();
		return;
	}
	if("uint32_t" == type) {
		MemAnalyzer::getInstance().findExact(
			static_cast<uint32_t>(searchValue_->text().toInt()));
	} else if("float" == type) {
		MemAnalyzer::getInstance().findExact(
			static_cast<float>(searchValue_->text().toInt()));
	} else if("uint64_t" == type) {
		MemAnalyzer::getInstance().findExact(
			static_cast<uint64_t>(searchValue_->text().toInt()));
	}

	size_t size = MemAnalyzer::getInstance().getResultsSize();
	resultsLable_->setText("Results found: " + QString::number(size));
	if(size == 0)
		findButton_->setEnabled(false);
	emit resultsUpdated();
}

void ExactSearchFrame::resetResults()
{
	MemAnalyzer::getInstance().reset();
	resultsLable_->setText("Results found: 0");
	findButton_->setEnabled(true);
	emit resultsUpdated();
}