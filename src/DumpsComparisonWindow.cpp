#include "DumpsComparisonWindow.hpp"
#include "DumpsComparisonTable.hpp"
#include "DumpsComparisonDelegate.hpp"
#include "DumpsComparisonHeader.hpp"
#include <QtWidgets>


DumpsComparisonWindow::DumpsComparisonWindow(QStringList files, QWidget* parent)
	: QDialog(parent)
{

	// QMenuBar* menuBar = new QMenuBar(this);
 //    QMenu* fileMenu = new QMenu("Add file");
 //    QMenu* settingsMenu = new QMenu("Settings");
 //    menuBar->addMenu(fileMenu);
 //    menuBar->addMenu(settingsMenu);
	// QToolBar *fileToolBar = new QToolBar("File");
	// QAction *newAct = new QAction("Add file", this);
	// fileToolBar->addAction(newAct);
	// QAction *newAct = new QAction("Settings", this);
	// fileToolBar->addAction(newAct);
	setWindowTitle ("Compare dumps");

	QVBoxLayout* mainLayout = new QVBoxLayout(this);

	QHBoxLayout* managmentLayout = new QHBoxLayout;
	mainLayout->addLayout(managmentLayout);

	alignmentCheckBox_ = new QCheckBox("Align to focus value");
	managmentLayout->addWidget(alignmentCheckBox_);
	connect(alignmentCheckBox_, SIGNAL(stateChanged(int)), this, SLOT(alignmentChanged(int)));


	highlightCheckBox_ = new QCheckBox("Highlight matches");
	managmentLayout->addWidget(highlightCheckBox_);
	connect(highlightCheckBox_, SIGNAL(stateChanged(int)), this, SLOT(highlightChanged(int)));


	table_ = new DumpsComparisonTable(this);
	connect(table_, SIGNAL(alignmentPossible(bool)), this, SLOT(onAlignmentStatusChanged(bool)));
	for(const QString file : files)
		table_->addFile(file);		

	tableView_ = new QTableView;
	mainLayout->addWidget(tableView_);
	tableView_->setModel(table_);
	tableView_->verticalHeader()->hide();
	tableView_->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	tableView_->setEditTriggers(QAbstractItemView::NoEditTriggers);
	tableView_->setSelectionMode(QAbstractItemView::NoSelection);
	tableView_->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	tableView_->setItemDelegate(new DumpsComparisonDelegate(this));
	// tableView_->horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);	// in DumpsComparisonHeader it doesn't work
	connect(tableView_, SIGNAL(clicked(const QModelIndex &)), this, SLOT(onTableClicked(const QModelIndex &)));	
	DumpsComparisonHeader* header = new DumpsComparisonHeader(this);
	tableView_->setHorizontalHeader(header);
	connect(header, &DumpsComparisonHeader::closeClicked, 
		[=](int col) {
			if(table_->columnCount() == 2)
			{
				close();
			} else {
				showAll(); // otherwise will be arbitrary bug
				table_->removeColumn(col);
				hide(0, table_->rowCount()/table_->typesNumber());
				updateWidth();
			}
		});


	slider_ = tableView_->verticalScrollBar();
	// ===============
	// should in synchro with showEvent
	if(table_->alignmentSupported())
	{
		table_->align();
		alignmentCheckBox_->setCheckState(Qt::Checked);
	} else {
		onAlignmentStatusChanged(false);
	}
	// ===============

	hide(0, table_->rowCount()/table_->typesNumber());
	updateWidth();
}

void DumpsComparisonWindow::showEvent(QShowEvent* event)
{
	QDialog::showEvent(event);
	// didn't find a better place to do that
	if(table_->aligned())
	{
		changeSliderPos(0.5);
	} else {
		changeSliderPos(0);
	}
	connect(slider_, SIGNAL(valueChanged(int)), this, SLOT(sliderChanged(int)));
}

void DumpsComparisonWindow::changeSliderPos(float proc)
{
	// proc in range [0, 1]
	slider_->setSliderPosition((slider_->maximum() - slider_->minimum())*proc);	
}

void DumpsComparisonWindow::onAlignmentStatusChanged(bool status)
{
	if(status)
	{
		alignmentCheckBox_->setEnabled(true);
	} else {
		alignmentCheckBox_->setEnabled(false);
		alignmentCheckBox_->setCheckState(Qt::Unchecked);
		QString error = "Not supported files:\n";
		for(QString file: table_->notSupportedFiles())
			error += "  " + file + "\n";
		error.chop(1);
		alignmentCheckBox_->setToolTip(error);
	}
}

void DumpsComparisonWindow::updateWidth()
{
	size_t entireWidth = table_->getColumnWidth(0);
	tableView_->horizontalHeader()->setSectionResizeMode(
			0, QHeaderView::Fixed);
	tableView_->setColumnWidth(0, entireWidth);
	for(int i=1; i<table_->columnCount(); i++)
	{
		tableView_->horizontalHeader()->setSectionResizeMode(
			i, QHeaderView::Stretch);
		entireWidth += table_->getColumnWidth(i);
	}
	entireWidth = (entireWidth > 320 ? entireWidth : 320);
	this->setFixedSize(entireWidth, 700);
}

void DumpsComparisonWindow::sliderChanged(int value)
{
	float posProc = (float)value/(slider_->maximum() - slider_->minimum());
	size_t rowCount = table_->rowCount();
	size_t rowLeft = posProc < 0.5 ? rowCount*posProc : rowCount*(1-posProc);

	size_t numRowsToInsert = 3; 		// move to future settings
	if(rowLeft < numRowsToInsert && posProc < 0.5) // insert at start
	{
		size_t inserted = table_->insertAtStart(numRowsToInsert);
		hide(0, inserted);
		slider_->setSliderPosition(inserted);	// magic bug, should be changed
	}
	if(rowLeft < numRowsToInsert && posProc > 0.5) // insert in the end
	{
		size_t inserted = table_->insertInTheEnd(numRowsToInsert);
		hide(table_->rowCount()/table_->typesNumber() - inserted, inserted);
	}
}

void DumpsComparisonWindow::hide(size_t start, size_t len)
{
	for(size_t i = start; i < start + len; i++)
		for(size_t n = 1; n < table_->typesNumber(); n++)
		{
			size_t row = i*table_->typesNumber() + n;
			if(!tableView_->isRowHidden(row))
				tableView_->hideRow(row);
		}
}

void DumpsComparisonWindow::showAll()
{
	for(size_t i = 0; i < table_->rowCount()/table_->typesNumber(); i++)
		for(size_t n = 1; n < table_->typesNumber(); n++)
		{
			size_t row = i*table_->typesNumber() + n;
			if(tableView_->isRowHidden(row))
				tableView_->showRow(row);
		}
}

void DumpsComparisonWindow::alignmentChanged(int value)
{
	if(value == Qt::Checked)
	{
		showAll(); // otherwise will be arbitrary bug
		table_->align();
		hide(0, table_->rowCount()/table_->typesNumber());
		changeSliderPos(0.5); // not always works properly
	} else if(value == Qt::Unchecked) {
		showAll();
		table_->breakAlignment();
		hide(0, table_->rowCount()/table_->typesNumber());
		changeSliderPos(0);
	}
}

void DumpsComparisonWindow::highlightChanged(int value)
{
	if(value == Qt::Checked)
	{
		table_->setHighlight(true);
	} else if(value == Qt::Unchecked) {
		table_->setHighlight(false);
	}	
}

void DumpsComparisonWindow::onTableClicked(const QModelIndex& index)
{
	if(!index.isValid())
		return;

	bool show = index.row() % table_->typesNumber() == 0 && \
		tableView_->isRowHidden(index.row() + 1);

	if(show)
	{	// show
		for(size_t i = 1; i <= table_->typesNumber(); i++)
		{
			tableView_->showRow(index.row() + i);
		}
	} else {	// hide
		size_t start = (index.row() - index.row() % table_->typesNumber());
		for(size_t i = 1; i < table_->typesNumber(); i++)
		{
			tableView_->hideRow(start + i);
		}
	}
		table_->layoutChanged();
}