#ifndef PEEPSLIT_EXACT_SEARCH_FRAME_H
#define PEEPSLIT_EXACT_SEARCH_FRAME_H

#include "ISearchFrame.hpp"

class QLineEdit;
class QComboBox;
class QPushButton;
class QLabel;

class ExactSearchFrame : public ISearchFrame {
	Q_OBJECT
public:
	ExactSearchFrame(QWidget* parent=nullptr);

public slots:
	void findResults() override;
	void resetResults() override;

private:
	QLineEdit* searchValue_;
	QComboBox* typeSelector_;
	QPushButton* findButton_;
	QLabel* resultsLable_;


	std::vector<std::string> _typesMap = {"uint32_t", "float", "uint64_t"};
};

#endif