#include "MemInspectorTable.hpp"
#include "ResultListWidgetItem.hpp"
#include "MemAnalyzer.hpp"
#include <QtWidgets>

#include <numeric>
#include <utility>



MemInspectorTable::MemInspectorTable(size_t seedAddr, QObject *parent, size_t initRowCount)
	: QAbstractTableModel(parent)
{
	heapStart_ = MemAnalyzer::getInstance().getHeapStart();
	heapEnd_ = heapStart_ + MemAnalyzer::getInstance().getHeapLen();
	size_t heapSize = heapEnd_ - heapStart_;
	rowsNumber_ = (initRowCount <  heapSize? initRowCount : heapSize);
	focusRow_ = (initRowCount/2 < seedAddr - heapStart_ ? 
		initRowCount/2 : seedAddr - heapStart_);
	baseAddr_ = seedAddr - focusRow_*sizeof(uint32_t);
	strData_ = collectData(baseAddr_, rowsNumber_);
}


QList<QStringList> MemInspectorTable::collectData(size_t addr, size_t count)
{
	QList<QStringList> newList;
	uint32_t* memData = new uint32_t[count];
	MemAnalyzer::getInstance().readMem(memData, addr, count);
	for(size_t r=0; r<count; r++)
	{
		QStringList rowList;
		for(int c=0; c<header_.size(); c++)
			rowList.append((this->*typeSelectionMap_[header_[c]])(addr + r*sizeof(uint32_t), &memData[r]));
		newList.append(rowList);
	}
	delete memData;
	return newList;
}

bool MemInspectorTable::addrInHeapRange(QString addr) const
{
	bool ok;
	size_t val = addr.toULong(&ok, 16);
	if(val >= heapStart_ && val < heapEnd_)
		return true;
	return false;
}

size_t MemInspectorTable::width() const
{
	return std::accumulate(std::begin(columnsWidth_),std::end(columnsWidth_), 0,
		[](const size_t previous, const auto& element)
		{ return previous + element.second; });
}

QString MemInspectorTable::getOffset(size_t addr, uint32_t*) const
{
	QString value = QString::number(addr - heapStart_);
	if(value.size() > 10)
	{
		value = "..." + value.right(10);
	}
	return value;
}

QString MemInspectorTable::getAddrHex(size_t addr, uint32_t*) const
{
	return "0x" + QString::number(addr, 16);
}

QString MemInspectorTable::getChar(size_t, uint32_t* value) const
{
	QString str;
	for(size_t i=0; i<sizeof(uint32_t); i++)
	{
		char ch = ((char*)value)[i];
		str += (ch < 33 || ch > 126) ? QChar(0x25AF) : ch;
		str += " ";
	}
	return str;
}

int MemInspectorTable::rowCount(const QModelIndex&) const
{
	return strData_.size();
}

int MemInspectorTable::columnCount(const QModelIndex&) const
{
	return strData_[0].size();
}


QVariant MemInspectorTable::data(const QModelIndex &index, int role) const
{
	if(!index.isValid() || role != Qt::DisplayRole)
		return QVariant();

	return strData_[index.row()][index.column()];
}

QString MemInspectorTable::getQStringValue(const QModelIndex &index) const
{
	if(!index.isValid())
		return QString("");

	return strData_[index.row()][index.column()];
}

QVariant MemInspectorTable::headerData(int section, Qt::Orientation orientation,
	int role) const
{
	if (role == Qt::DisplayRole && orientation == Qt::Horizontal)
	{
		return header_[section];
	}
	return QVariant();
}

size_t MemInspectorTable::insertAtStart(size_t count)
{
	count = (count < baseAddr_ - heapStart_ ?
		count : baseAddr_ - heapStart_);
	if(!count)
		return count;
	beginInsertRows(QModelIndex(), 0, count-1);
	baseAddr_ -= count * sizeof(uint32_t);
	QList<QStringList> newList = collectData(baseAddr_, count);
	strData_ = newList + strData_;
	focusRow_ += count;
	endInsertRows();
	return count;
}

size_t MemInspectorTable::insertInTheEnd(size_t count)
{
	size_t avalible = baseAddr_ + strData_.size() - heapStart_;
	count = (count < avalible ? count : avalible);
	if(!count)
		return count;
	beginInsertRows(QModelIndex(), strData_.size(), strData_.size()+count-1);
	QList<QStringList> newList = collectData(baseAddr_ + \
		strData_.size()*sizeof(uint32_t), count);
	strData_ += newList;
	endInsertRows();
	return count;
}

void MemInspectorTable::update()
{
	strData_ = collectData(baseAddr_, strData_.size());
	emit dataChanged(index(0, 0), index(strData_.size()-1, header_.size()-1));
}